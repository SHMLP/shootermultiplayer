using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class GunData : ScriptableObject
{
    #region Variables
    public string gunName;
    public GunRange nivel;
    public int maxAmmonCount, actualAmmon;
    public float reloadTime, damage, fireRate, range;
    public Vector2 recoil;
    public bool automatic;
    public Vector3 offsetPosition;

    public AudioClip[] reloadSounds;
    public AudioClip[] changeSounds;
    public AudioClip shootSound;


    #endregion

    public enum GunRange
    {
        Bajo,
        Medio,
        Alto

    }
}
