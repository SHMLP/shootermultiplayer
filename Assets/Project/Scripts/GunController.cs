using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GunController : MonoBehaviour
{
    #region Variables
    public PlayerController playerController;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0, maxGuns = 3;

    float lastShootTime = 0, lastReload = 0, lastChangeTime,lastGrabGunTime;
    public float returnSpeed = 1.5f, snappines = 1.2f, changeTime;
    Vector3 currentRotation, targetRotation;

    public GameObject crossHair;

    public LayerMask bullletHoleMask;

    public GameObject prefBulletHole;
    [SerializeField] float offsettBulletHole;

    public AudioSource sounds;

    bool reloading, isChangin, grabGun;

    #endregion

    #region Unity Functions
    private void Update()
    {
     
        if (actualGun!=null)
        {
            if (!reloading && !isChangin)
            {
                if (actualGun.data.actualAmmon > 0 )
                {
                    if (lastShootTime<=0)
                    {
                        if (!actualGun.data.automatic)
                        {
                            if (Input.GetButtonDown("Fire1"))
                            {
                                Shoot();
                            }
                        }
                        else
                        {
                            if (Input.GetButton("Fire1"))
                            {
                                Shoot();
                            }
                        }
                    }
                }
                if (Input.GetButtonDown("Reload"))
                {
                    if (actualGun.data.actualAmmon < actualGun.data.maxAmmonCount)
                    {
                        lastReload = 0;
                        reloading = true;
                        ReloadSound();
                    }
                }
            }
            if (!isChangin)
            {
            
                if (Input.GetButtonDown("Gun1"))
                {
                    

                    indexGun = 0;
                    if (guns[indexGun] != actualGun && guns[indexGun] != null)
                    {
                        actualGun.gameObject.SetActive(false);
                        isChangin = true;
                    }

                }
                if (Input.GetButtonDown("Gun2"))
                {
                   
                    indexGun = 1;
                    if (guns[indexGun] != actualGun && guns[indexGun] != null)
                    {
                        actualGun.gameObject.SetActive(false);
                        isChangin = true;
                    }
                }
                if (Input.GetButtonDown("Gun3"))
                {
                    

                    indexGun = 2;
                    if (guns[indexGun] != actualGun && guns[indexGun] != null)
                    {
                        actualGun.gameObject.SetActive(false);
                        isChangin = true;
                    }
                }
                if (Input.mouseScrollDelta.y != 0)
                {
                   

                    indexGun = (indexGun + ((guns.Length + (int)Mathf.Sign(Input.mouseScrollDelta.y)*(2- guns.Length)) / 2))%guns.Length;
                    if (guns[indexGun] != actualGun && guns[indexGun] != null)
                    {
                        actualGun.gameObject.SetActive(false);
                        isChangin = true;
                    }
                }
            }
        }

    

        if (lastShootTime>=0)
        {
            lastShootTime -= Time.deltaTime;
        }

        if (reloading)
        {
            
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }


        if (isChangin)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                ChangeGun(indexGun);
                isChangin = false;
            }
        }  
        
        if (grabGun)
        {
            lastGrabGunTime += Time.deltaTime;
            if (lastGrabGunTime >= 0.5f)
            {
                grabGun = false;
                lastGrabGunTime = 0;
            }
        }
        

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed*Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotation);



    }
    private void OnTriggerStay(Collider other)
    {
        
        if (Input.GetButtonDown("AgarrarObjetos") && !grabGun)
        {
            grabGun = true;
            other.GetComponent<BoxCollider>().enabled = false;
            if (actualGun != null)
            {
                bool ocupado = true;
                for (int i = 0; i < guns.Length; i++)
                {
                    if (guns[i] == null)
                    {
                        actualGun.gameObject.SetActive(false);
                        ocupado = false;
                        break;
                    }
                }
                if (ocupado)
                {
                    actualGun.transform.parent = null;
                    actualGun.transform.position = other.transform.position;
                    actualGun.transform.rotation = other.transform.rotation;
                    actualGun.GetComponent<BoxCollider>().enabled = true;
                }
            }
            else
            {
                crossHair.SetActive(true);
            }
            actualGun = other.GetComponent<Gun>();
            for (int i = 0; i < guns.Length; i++)
            {
                if (guns[i] == null)
                {
                    guns[i] = actualGun;
                    break;
                }
            }
            actualGun.transform.parent = playerController.gunPoint;
            actualGun.transform.localPosition = actualGun.data.offsetPosition;
            actualGun.transform.localRotation = Quaternion.identity;
            ChangeSound();
        }

    }



    #endregion

    #region Custom Funtion

    void Shoot()
    {
        if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, actualGun.data.range, bullletHoleMask))
        {
            if (hit.transform != null)
            {
                GameObject go = Instantiate(prefBulletHole,hit.point+(hit.normal* offsettBulletHole), Quaternion.LookRotation(hit.normal,Vector3.up));
                Destroy(go, 2f);
            }
        }
        actualGun.data.actualAmmon--;
        lastShootTime = actualGun.data.fireRate;
        actualGun.bulletEffect.gameObject.SetActive(true);
        actualGun.bulletEffect.Play();
        ShootSound();
        AddRecoil();
    }

    void AddRecoil()
    {

        targetRotation = new Vector3(-actualGun.data.recoil.x,Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y),0);
    }

    void Reload()
    {
        actualGun.data.actualAmmon = actualGun.data.maxAmmonCount;
    }
    void ChangeGun(int index)
    {
        actualGun.bulletEffect.gameObject.SetActive(false);
        ChangeSound();
        if (guns[index]!=null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
        lastChangeTime = 0;
    }

    public void ShootSound()
    {
        sounds.PlayOneShot(actualGun.data.shootSound);
    }
    public void ReloadSound()
    {
        sounds.PlayOneShot(actualGun.data.reloadSounds[Random.Range(0,actualGun.data.reloadSounds.Length)]);
    }
    public void ChangeSound()
    {
        sounds.PlayOneShot(actualGun.data.changeSounds[Random.Range(0,actualGun.data.changeSounds.Length)]);
    }
    #endregion
}
