using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSpawn : MonoBehaviour
{
    #region Variables

    public GameObject[] guns;
    public List<GameObject> gunsSpawn;
    [SerializeField]float speedRotation, spawnTime ;
    float lastSpawnTime;
    public GunData.GunRange rango;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        foreach (GameObject item in guns)
        {
            if (item.GetComponent<Gun>().data.nivel.ToString() == rango.ToString())
            {
                gunsSpawn.Add(item);
            }
        }
    }
    void Update()
    {
        if (transform.childCount==0)
        {
            lastSpawnTime += Time.deltaTime;
            if (lastSpawnTime>=spawnTime)
            {
                lastSpawnTime = 0;
                GameObject gun = Instantiate(gunsSpawn[Random.Range(0,gunsSpawn.Count)],transform.position,Quaternion.identity);
                gun.transform.parent = transform;

            }
        }
        else
        {
            transform.Rotate(Vector3.up*speedRotation*Time.deltaTime);
        }

    }

    #endregion
}
