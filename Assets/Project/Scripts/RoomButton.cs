using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using UnityEngine.UI;
public class RoomButton : MonoBehaviour
{
    public TMP_Text buttonNameRoom;
    RoomInfo info;
    public Button joinRoom;

    #region Custom Functions
    private void Awake()
    {
        joinRoom = buttonNameRoom.GetComponent<Button>();
        joinRoom.onClick.AddListener(delegate () { Launcher.Instance.JoinRoom(info); });
    }
    public void SetButtonDetails(RoomInfo inputInfo)
    {
        info = inputInfo;
        buttonNameRoom.text = info.Name;
    }
    #endregion
}
