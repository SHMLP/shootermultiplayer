using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables
    
    [SerializeField]internal Transform recoil, gunPoint;
    [SerializeField]internal Camera cam;

    [SerializeField] CharacterController controller;
    [SerializeField] Transform pointOfView;
    [SerializeField] float walkSpeed, runSpeed, jumpForce,gravityMod;

    float horizontalRotationStore, verticalRotationStore, actualSpeed;
    Vector3 direction,movement;
    Vector2 mouseInput;

    [Header("Ground Detection")]

    [SerializeField] bool isGrounded;
    [SerializeField] float radio, distance;
    [SerializeField] Vector3 offset;
    [SerializeField] LayerMask lm;

    #endregion

    #region Unity Functions
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        controller = GetComponent<CharacterController>();
        cam = Camera.main;
    }

    void Update()
    {
        Rotation();
        Movement();

    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position+offset,radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint,radio);
            Gizmos.DrawLine(transform.position + offset,endPoint);

            Gizmos.DrawSphere(hit.point,0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }

    }
    #endregion

    #region Functions

    bool IsGrounded()
    {
        isGrounded = false;
        if (Physics.SphereCast(transform.position+offset, radio,Vector3.down, out RaycastHit hit, distance,lm))
        {
            isGrounded = true;
        }
        return isGrounded;
    }
    void Movement()
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        float velY = movement.y;
        movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized*actualSpeed;
        movement.y = velY;
        movement.y += Physics.gravity.y*Time.deltaTime*gravityMod;

        if (Input.GetButton("Fire3"))
            actualSpeed = runSpeed;
        else
            actualSpeed = walkSpeed;


        if (IsGrounded() && Mathf.Abs(movement.y)>=jumpForce)
        {
            movement.y = 0;
        }

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            
            movement.y = jumpForce;
        }
        controller.Move(movement * Time.deltaTime);
        
    }
    void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"),Input.GetAxisRaw("Mouse Y"));
        horizontalRotationStore += mouseInput.x;
        
        verticalRotationStore += mouseInput.y;
        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60, 60);
        transform.rotation = Quaternion.Euler(0,horizontalRotationStore,0);
        pointOfView.localRotation = Quaternion.Euler(-verticalRotationStore, 0, 0);
    }
    #endregion
}
