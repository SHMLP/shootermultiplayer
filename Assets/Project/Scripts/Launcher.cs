using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;
    [SerializeField] GameObject[] screenObjects;

    public TMP_Text infoText, nameRoom, createRoomError;
    public TMP_InputField inputField;

    [SerializeField] RoomButton pref_RoomButton;
    [SerializeField] Transform contentScroll;
    [SerializeField] List<RoomButton> roomButtonList = new List<RoomButton>();
    #endregion

    #region Unity Functions

    private void Awake()
    {
        if (Instance==null)
            Instance = this;
        else
            Destroy(this);
    }

    private void Start()
    {
        infoText.text = "Conneting to network...";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
        
    }
    #endregion

    #region Custom Functions

    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i==index);
        }

    }

    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(inputField.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(inputField.text);
            infoText.text = "Creating Room...";
            SetScreenObjects(0);
        } 
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining room...";
        SetScreenObjects(0);
        
    }
    #endregion

    #region Photon

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }

    public override void OnJoinedRoom()
    {
        nameRoom.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(4);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        createRoomError.text = "Create room failed:" + message;
        SetScreenObjects(5);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {

        SetScreenObjects(1);
    }

    
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                bool roomExistente = false;
                for (int j = 0; j < roomButtonList.Count; j++)
                {
                    if (roomButtonList[j].buttonNameRoom.text == roomList[i].Name)
                    {
                        roomExistente = true;
                    }
                }
                if (!roomExistente)
                {
                    RoomButton newRoomButton = Instantiate(pref_RoomButton,contentScroll);
                    newRoomButton.SetButtonDetails(roomList[i]);
                    roomButtonList.Add(newRoomButton);
                }
            }
            if(roomList[i].RemovedFromList)
            {
                for (int j = 0; j < roomButtonList.Count; j++)
                {
                    if (roomButtonList[j].buttonNameRoom.text == roomList[i].Name)
                    {
                        Destroy(roomButtonList[j].gameObject);
                        roomButtonList.Remove(roomButtonList[j]);
                    }
                }     
            }
        }
    }
    #endregion

}

